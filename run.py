#!/usr/bin/env python

import poc

app = poc.create_application()

if __name__ == "__main__":
    app.run(debug=True)
