import flask_wtf
import wtforms.fields.html5
import wtforms.validators

class BloodValueForm(flask_wtf.FlaskForm):
    cAP = wtforms.fields.html5.DecimalField()
