import flask
from poc.main import main
from poc.main.forms import BloodValueForm

@main.route('/')
def index():
    blood_value_form = BloodValueForm()
    return flask.render_template('main.html', form=blood_value_form)
