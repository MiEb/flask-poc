import flask

main = flask.Blueprint('main', __name__, template_folder='templates')

import poc.main.forms
import poc.main.routes
