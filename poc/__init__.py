import flask
import poc.main

def create_application():
    app = flask.Flask(__name__)
    app.config['SECRET_KEY'] = "Voll geiler Secrekt Key"
    # Register module blueprints
    app.register_blueprint(poc.main.main)
    return app
